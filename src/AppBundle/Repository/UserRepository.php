<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getUserNotAdmin()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        // get all meals group by day
        $qb->select('u')
            ->from('AppBundle\Entity\User', 'u')
            ->where('u.roles NOT LIKE :roles')
            ->setParameter('roles', '%ROLE_ADMIN%')
            ->orderBy('u.username', 'ASC');
            
        return $qb->getQuery()->getResult();
    }
}