<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="meal", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"meal_date", "meal_time", "user_id"})
 * })
 * @UniqueEntity(
 *     fields={"meal_date", "meal_time", "user"},
 *     message="This meal already exist."
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MealRepository")
 */
class Meal
{
    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /** @ORM\Column(type="date") */
    protected $meal_date;

    /** @ORM\Column(type="time") */
    protected $meal_time;

    /** @ORM\Column(type="text",length=255)) */
    protected $description;

    /** @ORM\Column(type="integer", length=11) */
    protected $calories;
     
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    public function getCalories()
    {
        return $this->calories;
    }

    public function setCalories($calories)
    {
        $this->calories = $calories;
    }
    public function getId() {
        return $this->id;
    }

    public function getMealDate() {
        return $this->meal_date;
    }

    public function getMealTime() {
        return $this->meal_time;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setMealDate($meal_date) {
        $this->meal_date = $meal_date;
    }

    public function setMealTime($meal_time) {
        $this->meal_time = $meal_time;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    function getUser() 
    {
        return $this->user;
    }

    function setUser($user) 
    {
        $this->user = $user;
    }
}