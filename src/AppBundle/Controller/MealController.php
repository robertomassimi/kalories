<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Meal;
use AppBundle\Form\MealType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;


class MealController extends Controller
{
    
    /**
     * @Route("/meal/add", name="meal_add")
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $meal = new Meal();
            
        $form = $this->createForm(MealType::class, $meal);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $meal->setUser($this->getUser());
            $em->persist($meal);
            try {
                $em->flush();
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash(
                    'danger',
                    'meal already exist!'
                ); 
            }
            return $this->redirectToRoute('meal_index');
        }

        return $this->render(
            'meal/add.html.twig', 
            [
                'form' => $form->createView()
            ]
       );
    }    
    
    /**
     * @Route("/meal/index", name="meal_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $defaultData = [];
        $form = $this->createFormBuilder($defaultData)
            ->add('from', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'required' => false
            ))
            ->add('to', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'required' => false
            ))
            ->getForm();
        $form->handleRequest($request);        
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $from = $form['from']->getData();
            $to = $form['to']->getData();
            $meals = $em->getRepository(Meal::class)->groupByDate($this->getUser()->getId(),$from,$to);
        }        
        else
        {
            $meals = $em->getRepository(Meal::class)->groupByDate($this->getUser()->getId());
        }
        
        return $this->render(
                'meal/index.html.twig',
                [
                    'meals' => $meals,
                    'form'  => $form->createView()
                ]
        );		
    }
    
    /**
     * @Route("/meal/{id}/edit", name="meal_edit")
     */
    public function editAction(Request $request, Meal $meal)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(MealType::class, $meal,  array(
                'validation_groups' => array('edit')
            ));
         
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $em->flush();
            return $this->redirectToRoute('meal_index');
        }
 
        return $this->render('meal/edit.html.twig', [
            'form' => $form->createView(),
            'meal_id' => $meal->getId()
        ]);         
         
    }    

    /**
     * @Route("/meal/{id}/del", name="meal_del")
     */
    public function delAction(Request $request, Meal $meal)
    {
        if (!$meal) 
        {
            $this->addFlash(
                'danger',
                'Meal is empty!'
            );
            return $this->redirectToRoute('meal_index');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($meal);
        $em->flush();

        $this->addFlash(
            'success',
            'Successfull delete meal!'
        );        
        return $this->redirectToRoute('meal_index');
    }
    
    
}