Kalories Application

# What is Kalories?

> Kalories is an application to track your meals and calories.

# Installation

> download and install application, create test users and start server

 1.  git clone https://robertomassimi@bitbucket.org/robertomassimi/kalories.git
 2.  cd kalories
 3.  composer up
 4.  php bin/console doctrine:database:create
 5.  php bin/console doctrine:schema:update --force
 6.  php bin/console fos:user:create testadmin test@example.com p@ssword
 7.  php bin/console fos:user:promote testadmin ROLE_ADMIN
 8.  php bin/console fos:user:create testuser1 test1@example.com p@ssword
 9.  php bin/console fos:user:create testuser2 test2@example.com p@ssword
 10. php bin/console fos:user:create testuser3 test3@example.com p@ssword
 11. php bin/console server:start
 12. open url http://127.0.0.1:8000
 13. login with user admin created: username: testadmin password: p@ssword
 14. login with normal user created: username: testuser1 password: p@ssword


# Requirements

- Php >= 5.5.9

# Features

- set the expected number of calories per day 
- list of my meals and calories
- the total calories for that day is colored in green if it's below the expected number, otherwise it is red.
- input a meal with relative calories
- edit meal
- delete meal
- filter meals by date (from-to)

# Usage manual
As admin user you can open Settings>>Calories and set the expected number of calories per day for user and show list of user and calories stored.

As normal user you can open Meal from menu and you able to:

- add a new meal with calories: insert day, time (breakfast,lunch or dinner), description (fish,meat..), calories of this meal

- search meal by date

- show detail of mail group by day

- edit single meal

- in edit meal, you able o delete it

From menu user (at top, right) you can logout from application
