<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class UsercaloriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // prepare dropdown select with only user to set user's calories
        $builder->add('id', EntityType::class, array(
            'placeholder' => 'Select a User...',
            'class' => 'AppBundle:User',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.roles NOT LIKE :roles')
                    ->setParameter('roles', '%ROLE_ADMIN%')
                    ->orderBy('u.username', 'ASC');
            },
            'choice_label' => 'username',
            'label' => 'Username',
            'empty_data' => null,
        ))
        ->add('calories', NumberType::class);
    }

}
