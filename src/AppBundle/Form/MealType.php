<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MealType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mealTime = $builder->getData()->getMealTime();
        
        // time of meal is breakfast,launch or dinner. Prepare dropdown select
        $_times = [
            'select' => 'Choose...',
            'breakfast' => '08:00:00',
            'launch' => '13:00:00',
            'dinner' => '20:00:00',
        ];
        $times = [];
        foreach( $_times as $label => $time )
        {
            $dt = \DateTime::createFromFormat("H:i:s", $time);
            $times[$time] = $dt;
        }

        $choices = ['choices' => $times];
        if(isset($mealTime))
        {
            $choices['preferred_choices'] = [$times[$mealTime->format("H:i:s")]];
        }
        
        $preferredChoices = (isset($mealTime)?:0);
        
        $builder
            ->add('meal_date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('meal_time', ChoiceType::class, $choices)
            ->add('description', TextareaType::class, array(
                'attr' => array('class' => 'tinymce'),
            ))
            ->add('calories', NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Meal',
        ]);
    }
}
