<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MealRepository extends EntityRepository
{
    public function groupByDate($userId,$from=null,$to=null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        // get all meals group by day
        $qb->select('meal.meal_date,
                breakfast.id as breakfast_id,
                launch.id as launch_id,
                dinner.id as dinner_id,
                breakfast.description as breakfast_data,
                launch.description as launch_data,
                dinner.description as dinner_data,
                (COALESCE(breakfast.calories,0)+COALESCE(launch.calories,0)+COALESCE(dinner.calories,0)) as calories
                    ')
            ->from('AppBundle\Entity\Meal', 'meal')
            ->leftJoin(
                'AppBundle\Entity\Meal',
                'breakfast',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('breakfast.meal_date', 'meal.meal_date'),
                    $qb->expr()->eq('breakfast.meal_time', ':breakfast')
                )
            )
            ->leftJoin(
                'AppBundle\Entity\Meal',
                'launch',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('meal.meal_date', 'launch.meal_date'),
                    $qb->expr()->eq('launch.meal_time', ':launch')
                )
            )
            ->leftJoin(
                'AppBundle\Entity\Meal',
                'dinner',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('meal.meal_date', 'dinner.meal_date'),
                    $qb->expr()->eq('dinner.meal_time', ':dinner')
                )
            )
            ->setParameters(
                array(
                    'breakfast' => "08:00:00",
                    'launch' => "13:00:00",
                    'dinner' => "20:00:00"
                ))
            ->where('meal.user = :user_id');
        
            if($from)
            {
                $qb->andWhere('meal.meal_date >= :from')
                    ->setParameter('from', $from->format('Y-m-d') . ' 00:00:00');
            }

            if($to)
            {
                $qb->andWhere('meal.meal_date <= :to')
                    ->setParameter('to', $to->format('Y-m-d') . ' 00:00:00');
            }
        
            $qb->setParameter('user_id', $userId)
            ->groupBy('meal.meal_date');
            
        return $qb->getQuery()->getResult();
    }
}