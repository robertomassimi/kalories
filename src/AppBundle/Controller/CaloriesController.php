<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\UsercaloriesType;
use Symfony\Component\HttpFoundation\JsonResponse;
class CaloriesController extends Controller
{
    /**
     * Save calories to user
     * @Route("/admin/set-calories", name="set_calories")
     */
    public function setCaloriesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->get("usercalories");
        
        if( isset($data['id']) && $data['id'] )
        {
            $user = $em->getRepository('AppBundle:User')->find($data['id']);
        }
        else
        {
            $user = new User();
        }
            
        $form = $this->createForm(UsercaloriesType::class, $user);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $em->persist($user);
            $em->flush();
            $this->addFlash(
                'success',
                'Successfull setting calories!'
            );
        }

        $users = $em->getRepository(User::class)->getUserNotAdmin();
        return $this->render(
            'user/set_calories.html.twig', 
            [
                'form' => $form->createView(),
                'users' => $users
            ]
       );
    }
    
    /**
     * return json with calories of user for day
     * @Route("/api/get-calories-by-user", name="api_get_calories_by_user")
     */
    public function getCaloriesByUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository("AppBundle:User");
        
        $user = $userRepository->createQueryBuilder("q")
            ->where("q.id = :userid")
            ->setParameter("userid", $request->query->get("userid"))
            ->getQuery()
            ->getOneOrNullResult();
        
        return new JsonResponse(['calories' => $user->getCalories()]);
    }    
    
}
